db.fruits.aggregate([
				{
					$group: 
						{
							_id:"$onSale",
							fruitsOnSale : {
								$count: {}
							}
						}
				}
		])

db.fruits.aggregate([
            {$match: {$and:[{onSale: true},{stock:{$gte:20}}]}},
                {
                    $group: 
                        {
                            _id:"$onSale",
                            enoughStock : {
                                $count: {}
                            }
                        }
                }
        ])

db.fruits.aggregate([
				{
					$group: 
						{
							_id:"$supplier_id",
							avg_price : {
								$avg: "$price"
							}
						}
				}
		])

db.fruits.aggregate([
				{
					$group: 
						{
							_id:"$supplier_id",
							max_price : {
								$max: "$price"
							}
						}
				}
		])

db.fruits.aggregate([
				{
					$group: 
						{
							_id:"$supplier_id",
							min_price : {
								$min: "$price"
							}
						}
				}
		])